package com.tho.serialization;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.tho.serialization.serializers.BitSerializerTest;
import com.tho.serialization.serializers.JsonSimpleSerializerTest;
import com.tho.serialization.serializers.XmlDomSerializerTest;
import com.tho.serialization.type.BitBufferTest;
import com.tho.serialization.type.StackTest;
import com.tho.serialization.util.BitUtilsTest;
import com.tho.serialization.util.MathUtilsTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
		BitBufferTest.class,
		StackTest.class,

		BitSerializerTest.class,
		JsonSimpleSerializerTest.class,
		XmlDomSerializerTest.class,

		BitUtilsTest.class,
		MathUtilsTest.class
})
public class AllUnitTests {
	private AllUnitTests() {
		// Nothing to do
	}
}
