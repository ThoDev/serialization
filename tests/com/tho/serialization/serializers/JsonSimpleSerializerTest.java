package com.tho.serialization.serializers;

public class JsonSimpleSerializerTest extends AbstractSerializerTest {

	private final AbstractSerializer jsonSimpleSerializer = new JsonSimpleSerializer();

	@Override
	protected AbstractSerializer getSerializer() {
		return this.jsonSimpleSerializer;
	}
}
