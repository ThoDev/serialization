package com.tho.serialization.serializers;

public class XmlDomSerializerTest extends AbstractSerializerTest {

	private final AbstractSerializer xmlDomSerializer = new XmlDomSerializer();

	@Override
	protected AbstractSerializer getSerializer() {
		return this.xmlDomSerializer;
	}
}
