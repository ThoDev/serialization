package com.tho.serialization.serializers;

public class BitSerializerTest extends AbstractSerializerTest {

	private final AbstractSerializer bitSerializer = new BitSerializer(8 * 2048);

	@Override
	protected AbstractSerializer getSerializer() {
		return this.bitSerializer;
	}
}
